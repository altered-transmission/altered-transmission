# Altered Transmission

This is the source for the Half-Life 2 modification Altered Transmission. It
requires the Source SDK Base 2013 Singleplayer to run.

## Linux

To build on Linux:

    cd sp/src/game
    ./creategameprojects
    CC=gcc CXX=g++ DEFINES='-Wno-narrowing -Wno-ignored-attributes -DALTERED_TRANSMISSION -DFINITE_COMPAT' make -C game/client -f client_linux32_episodic.mak
	CC=gcc CXX=g++ DEFINES='-Wno-narrowing -Wno-ignored-attributes -DALTERED_TRANSMISSION -DFINITE_COMPAT' make -C game/server -f server_linux32_episodic.mak

The top level makefile will also rebuild some of the public .a files that have
source code available, but they don't need to be rebuilt and rebuilding with a
newer glibc results in linker symbol issues. Instead the client.so and server.so
files are built directly.

The `CC` and `CXX` variables don't need to be set, but it will use ccache by
default which will use a lot of disk space in `~/.ccache`.

The `ALTERED_TRANSMISSION` define is needed is enable customizations.

The `FINITE_COMPAT` define is needed on modern glibc to provide compatibility
replacements for removed glibc symbols such as `__acosf_finite`.

Use `MAKE_VERBOSE=1` for verbose builds to debug errors.

If the filesystem is low on disk space but another filesystem has more space:

	mkdir -p /tmp/obj_client_episodic_linux32
	mkdir -p /tmp/obj_server_episodic_linux32
	ln -s /tmp/obj_client_episodic_linux32 sp/src/game/client/obj_client_episodic_linux32
	ln -s /tmp/obj_server_episodic_linux32 sp/src/game/server/obj_server_episodic_linux32

Technically one is supposed to use the Steam Runtime to build the mod against
the system libraries of the lowest supported platform, which would probably fix
the various errors that happens on newer Linux distributions.

The maps cannot be compiled on Linux. Compile them on Windows and put them in
the `maps` directory using e.g. a symbolic link to the Windows filesystem.

To install, copy `sp/game/mod_episodic` to
`~/.local/share/Steam/steamapps/sourcemods/Altered Transmission`.

Steam on Linux doesn't support symbolic links inside sourcemods. However, one
can make an `Altered Transmission` and symlink all the contents of
`mod_episodic` into it including the bin directory. This approach uses the
latest git version and keeps the game state out of the git repository.

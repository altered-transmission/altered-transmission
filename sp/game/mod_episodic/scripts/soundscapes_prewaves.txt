// ==============================================================================================
// This Script is modified by Sortie and is used for his mod, Altered Transmission.
// Chapter 1: Prewaves
// ==============================================================================================

// Soundscapes for Canals & Underground

// city voice

// pa requests player id

//  CHAR_STREAM			'*'		// as one of 1st 2 chars in name, indicates streaming wav data
//  CHAR_USERVOX		'?'		// as one of 1st 2 chars in name, indicates user realtime voice data
//  CHAR_SENTENCE		'!'		// as one of 1st 2 chars in name, indicates sentence wav
//  CHAR_DRYMIX			'#'		// as one of 1st 2 chars in name, indicates wav bypasses dsp fx
//  CHAR_DOPPLER		'>'		// as one of 1st 2 chars in name, indicates doppler encoded stereo wav: left wav (incomming) and right wav (outgoing).
//  CHAR_DIRECTIONAL	'<'		// as one of 1st 2 chars in name, indicates stereo wav has direction cone: mix left wav (front facing) with right wav (rear facing) based on soundfacing direction
//  CHAR_DISTVARIANT	'^'		// as one of 1st 2 chars in name, indicates distance variant encoded stereo wav (left is close, right is far)
//  CHAR_OMNI			'@'		// as one of 1st 2 chars in name, indicates non-directional wav (default mono or stereo)
//  CHAR_SPATIALSTEREO	')'		// as one of 1st 2 chars in name, indicates spatialized stereo wav
//  CHAR_FAST_PITCH		'}'		// as one of 1st 2 chars in name, forces low quality, non-interpolated pitch shift

// Announcements inside the trains tation.
"cityvoice_level_trainstation"
{
	"playrandom"
	{
		"time"		"10,180"
		"volume"	"0.5,1.0"
		"pitch"		"90,115"
		"position"	"random"
		"soundlevel"	"SNDLVL_140dB"
		"rndwave"
		{
			"wave"	"trainstation\announce.wav"
		}
	}
}

// The trainstation platform.
"at_01.platform"
{
	"dsp"	"1"
	"playsoundscape"
	{
		"name"		"d1_trainstation.Platform"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"cityvoice_level_trainstation"
		"volume"	"1.0"
	}
}

// The trainstation escalators.
"at_01.escalator"
{
	"dsp"	"1"
	"playsoundscape"
	{
		"name"		"d1_trainstation.Turnstyle"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"cityvoice_level_trainstation"
		"volume"	"1.0"
	}
}

// Inside the trainstation.
"at_01.inside"
{
	"dsp"	"1"
	"playsoundscape"
	{
		"name"		"d1_trainstation.Turnstyle"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"cityvoice_level_trainstation"
		"volume"	"1.0"
	}
}

// Inside the trainstation near a combine computer.
"at_01.inside_computer"
{
	"dsp"	"1"
	"playsoundscape"
	{
		"name"		"at_01.inside"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"combine.computer"
		"volume"	"1.0"
		"position" 	"0"
	}
}



// One the player leaves the trainstation and enters the plaza.
"prewaves_00_plaza"
{
	"dsp"	"1"


	"playsoundscape"
	{
		"name"		"d1_canals.util_windgusts"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"d1_canals.util_shoreline"
		"volume"	"0.15"
	}
	"playlooping"
	{
		"volume"	"0.4"
		"wave"		"ambient/atmosphere/plaza_amb.wav"
		"pitch"		"100"
	}
	"playrandom"
	{
		"time"		"15,35"
		"volume"	"0.15,0.4"
		"pitch"		"90,115"
		"position"	"random"
		"soundlevel"	"SNDLVL_140dB"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/apc_distant1.wav"
			"wave"	"ambient/levels/streetwar/apc_distant2.wav"
			"wave"	"ambient/levels/streetwar/apc_distant3.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant1.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant2.wav"
			"wave"	"ambient/levels/streetwar/heli_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant1.wav"
			"wave"	"ambient/levels/streetwar/strider_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant3.wav"
			"wave"	"ambient/levels/streetwar/strider_distant_walk1.wav"
		}

	}
	"playrandom"
	{
		"time"		"60,300"
		"volume"	"0.15,0.2"
		"pitch"		"90,105"
		"soundlevel"	"SNDLVL_140dB"

		"position"	"random"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/city_chant1.wav"
			"wave"	"ambient/levels/streetwar/city_riot1.wav"
			"wave"	"ambient/levels/streetwar/city_riot2.wav"
			"wave"	"ambient/levels/streetwar/city_scream3.wav"
		}

	}
}

"hq_junction"
{
	"dsp"	"1"

	"playlooping"
	{
		"volume"	"0.10"
		"wave"		"ambient/atmosphere/sewer_air1.wav"
		"pitch"		"100"
	}
	"playlooping"
	{
		"volume"	"0.3"
		"wave"		"ambient/atmosphere/indoor1.wav"
		"pitch"		"100"
	}
	"playlooping"
	{
		"volume"	"0.35"
		"wave"		"ambient/levels/canals/waterleak_loop1.wav"
		"pitch"		"100"
	}
}

"hq_sewer"
{
	"dsp"	"1"
	"playlooping"
	{
		"volume"	"0.10"
		"wave"		"ambient/atmosphere/sewer_air1.wav"
		"pitch"		"100"
	}
	"playlooping"
	{
		"volume"	"0.35"
		"wave"		"ambient/levels/canals/waterleak_loop1.wav"
		"pitch"		"100"
	}
	"playlooping"
	{
		"volume"	"0.1"
		"wave"		"ambient/water/corridor_water.wav"
		"pitch"		"100"
	}
	"playrandom"
	{
		"time"		"0.7,3.7"
		"volume"	"0.3,0.7"
		"pitch"		"100"

		"rndwave"
		{
			"wave"	"*ambient/water/distant_drip1.wav"
			"wave"	"*ambient/water/distant_drip2.wav"
			"wave"	"*ambient/water/distant_drip3.wav"
			"wave"	"*ambient/water/distant_drip4.wav"
		}
	}
	"playrandom"
	{
		"time"		"6,18"
		"volume"	"0.3,0.4"
		"pitch"		"100"

		"rndwave"
		{
			"wave"	"ambient/creatures/flies4.wav"
			"wave"	"ambient/creatures/flies5.wav"
			"wave"	"ambient/creatures/rats1.wav"
			"wave"	"ambient/creatures/rats2.wav"
			"wave"	"ambient/creatures/rats3.wav"
			"wave"	"ambient/creatures/rats4.wav"
		}
	}
}

"prewaves_03_parking"
{
	"dsp"	"1"

	"playlooping"
	{
		"volume"	"0.15"
		"wave"		"ambient/atmosphere/indoor1.wav"
		"pitch"		"100"
	}
	"playlooping"
	{
		"volume"	"0.1"
		"wave"		"ambient/atmosphere/sewer_air1.wav"
		"pitch"		"100"
	}
	"playlooping"
	{
		"volume"	"0.1"
		"wave"		"ambient/atmosphere/tunnel1.wav"
		"pitch"		"100"
	}
	"playrandom"
	{
		"time"		"10,25"
		"volume"	"0.4,0.7"
		"pitch"		"100"

		"rndwave"
		{
			"wave"	"ambient/creatures/flies1.wav"
			"wave"	"ambient/creatures/flies2.wav"
			"wave"	"ambient/creatures/flies3.wav"
			"wave"	"ambient/creatures/flies4.wav"
			"wave"	"ambient/creatures/flies5.wav"
			"wave"	"ambient/creatures/rats1.wav"
			"wave"	"ambient/creatures/rats2.wav"
			"wave"	"ambient/creatures/rats3.wav"
			"wave"	"ambient/creatures/rats4.wav"
		}
	}
}

// Almost no caos, and no distant fighting. Wind, low water.

"prewaves_03_exit_parking"
{
	"dsp"	"1"

	"playlooping"
	{
		"volume"	"0.15"
		"wave"		"ambient/atmosphere/indoor1.wav"
		"pitch"		"100"
	}
	"playsoundscape"
	{
		"name"		"d1_canals.util_windgusts"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"d1_canals.util_shoreline"
		"volume"	"0.1"
	}
	"playlooping"
	{
		"volume"	"0.1"
		"wave"		"ambient/atmosphere/plaza_amb.wav"
		"pitch"		"100"
	}
	"playrandom"
	{
		"time"		"20,80"
		"volume"	"0.10,0.25"
		"pitch"		"90,115"
		"position"	"random"
		"soundlevel"	"SNDLVL_140dB"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/apc_distant1.wav"
			"wave"	"ambient/levels/streetwar/apc_distant2.wav"
			"wave"	"ambient/levels/streetwar/apc_distant3.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant1.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant2.wav"
			"wave"	"ambient/levels/streetwar/heli_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant1.wav"
			"wave"	"ambient/levels/streetwar/strider_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant3.wav"
			"wave"	"ambient/levels/streetwar/strider_distant_walk1.wav"
		}

	}

}

// Some caos, and very distant fighting. Wind, water, and toxic slime.

"prewaves_03_entrance_parking"
{
	"dsp"	"1"

	"playsoundscape"
	{
		"name"		"city_searching_level1"
		"volume"	"0.7"
	}

	"playsoundscape"
	{
		"name"		"d1_canals.util_windgusts"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"d1_canals.util_shoreline"
		"volume"	"0.15"
	}
	"playsoundscape"
	{
		"name"		"d1_canals.util_toxic_slime"
		"volume"	"0.3"
	}
	"playlooping"
	{
		"volume"	"0.3"
		"wave"		"ambient/atmosphere/plaza_amb.wav"
		"pitch"		"100"
	}
	"playrandom"
	{
		"time"		"20,70"
		"volume"	"0.15,0.3"
		"pitch"		"90,115"
		"position"	"random"
		"soundlevel"	"SNDLVL_140dB"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/apc_distant1.wav"
			"wave"	"ambient/levels/streetwar/apc_distant2.wav"
			"wave"	"ambient/levels/streetwar/apc_distant3.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant1.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant2.wav"
			"wave"	"ambient/levels/streetwar/heli_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant1.wav"
			"wave"	"ambient/levels/streetwar/strider_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant3.wav"
			"wave"	"ambient/levels/streetwar/strider_distant_walk1.wav"
		}
	}
	"playrandom"
	{
		"time"		"60,300"
		"volume"	"0.15,0.2"
		"pitch"		"90,105"
		"soundlevel"	"SNDLVL_140dB"
		"position"	"random"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/city_chant1.wav"
			"wave"	"ambient/levels/streetwar/city_riot1.wav"
			"wave"	"ambient/levels/streetwar/city_riot2.wav"
			"wave"	"ambient/levels/streetwar/city_scream3.wav"
		}

	}
	"playrandom"
	{
		"time"		"10, 23"
		"volume"	"0.3,0.8"
		"pitch"		"90,105"
		"soundlevel"	"SNDLVL_140dB"
		"position"	"random"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/city_battle1.wav"
			"wave"	"ambient/levels/streetwar/city_battle2.wav"
			"wave"	"ambient/levels/streetwar/city_battle3.wav"
			"wave"	"ambient/levels/streetwar/city_battle4.wav"
			"wave"	"ambient/levels/streetwar/city_battle5.wav"
			//"wave"	"ambient/levels/streetwar/city_battle6.wav"
			"wave"	"ambient/levels/streetwar/city_battle7.wav"
			"wave"	"ambient/levels/streetwar/city_battle8.wav"
			"wave"	"ambient/levels/streetwar/city_battle9.wav"
			//"wave"	"ambient/levels/streetwar/city_battle10.wav"
			//"wave"	"ambient/levels/prison/inside_battle1.wav"
			//"wave"	"ambient/levels/prison/inside_battle2.wav"
			//"wave"	"ambient/levels/prison/inside_battle3.wav"
			//"wave"	"ambient/levels/prison/inside_battle4.wav"
		}

	}
}

// More caos, and closer fighting. Wind, water, and closer toxic slime. Citadel Alert


"prewaves_03_magasin"
{
	"dsp"	"1"
	"playlooping"
	{
		"volume"	"1.0"
		"wave"		"ambient/alarms/citadel_alert_loop2.wav"
		"pitch"		"100"
	}
	"playlooping"
	{
		"volume"	"0.4"
		"wave"		"ambient/atmosphere/plaza_amb.wav"
		"pitch"		"100"
	}
	"playsoundscape"
	{
		"name"		"city_searching_level1"
		"volume"	"0.8"
	}

	"playsoundscape"
	{
		"name"		"d1_canals.util_windgusts"
		"volume"	"1.0"
	}
	"playsoundscape"
	{
		"name"		"d1_canals.util_shoreline"
		"volume"	"0.10"
	}
	"playsoundscape"
	{
		"name"		"d1_canals.util_toxic_slime"
		"volume"	"0.4"
	}
	"playrandom"
	{
		"time"		"25,45"
		"volume"	"0.15,0.25"
		"pitch"		"90,115"
		"soundlevel"	"SNDLVL_140dB"
		"position"	"random"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/apc_distant1.wav"
			"wave"	"ambient/levels/streetwar/apc_distant2.wav"
			"wave"	"ambient/levels/streetwar/apc_distant3.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant1.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant2.wav"
			"wave"	"ambient/levels/streetwar/heli_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant1.wav"
			"wave"	"ambient/levels/streetwar/strider_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant3.wav"
			"wave"	"ambient/levels/streetwar/strider_distant_walk1.wav"
		}

	}
	"playrandom"
	{
		"time"		"45,250"
		"volume"	"0.15,0.2"
		"pitch"		"90,105"
		"soundlevel"	"SNDLVL_140dB"
		"position"	"random"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/city_chant1.wav"
			"wave"	"ambient/levels/streetwar/city_riot1.wav"
			"wave"	"ambient/levels/streetwar/city_riot2.wav"
			"wave"	"ambient/levels/streetwar/city_scream3.wav"
		}
	}
}

"prewaves_03_magasin_interior"
{
	"dsp"	"1"

	"playlooping"
	{
		"volume"	"0.3"
		"wave"		"hl1/ambience/alien_powernode.wav"
		"pitch"		"100"
	}
	"playsoundscape"
	{
		"name"		"streetwar.util_combine_atmosphere"
		"volume"	"0.7"
	}
	"playlooping"
	{
		"volume"	"0.55"
		"wave"		"ambient/atmosphere/indoor1.wav"
		"pitch"		"100"
	}

	"playrandom"
	{
		"time"		"25,45"
		"volume"	"0.1,0.25"
		"pitch"		"90,115"
		"soundlevel"	"SNDLVL_140dB"

		"position"	"random"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/apc_distant1.wav"
			"wave"	"ambient/levels/streetwar/apc_distant2.wav"
			"wave"	"ambient/levels/streetwar/apc_distant3.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant1.wav"
			"wave"	"ambient/levels/streetwar/gunship_distant2.wav"
			"wave"	"ambient/levels/streetwar/heli_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant1.wav"
			"wave"	"ambient/levels/streetwar/marching_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant1.wav"
			"wave"	"ambient/levels/streetwar/strider_distant2.wav"
			"wave"	"ambient/levels/streetwar/strider_distant3.wav"
			"wave"	"ambient/levels/streetwar/strider_distant_walk1.wav"
		}
	}
	"playrandom"
	{
		"time"		"45,250"
		"volume"	"0.15,0.2"
		"pitch"		"90,105"
		"soundlevel"	"SNDLVL_140dB"
		"position"	"random"
		"rndwave"
		{
			"wave"	"ambient/levels/streetwar/city_chant1.wav"
			"wave"	"ambient/levels/streetwar/city_riot1.wav"
			"wave"	"ambient/levels/streetwar/city_riot2.wav"
			"wave"	"ambient/levels/streetwar/city_scream3.wav"
		}
	}
}

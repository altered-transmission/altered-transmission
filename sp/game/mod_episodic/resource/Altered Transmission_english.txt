"lang" 
{ 
"Language" "English" 
"Tokens" 
{ 
"altered transmission_Chapter1_Title"		"PREWAVES"
"altered transmission_Chapter2_Title"		"CONSEQUENCE"
"altered transmission_Chapter3_Title"		"UNDERGROUND RELATIONSHIPS"
"altered transmission_Chapter4_Title"		"EVASION"
"altered transmission_Chapter5_Title"		"ARTIFICIAL REALITY"

"hl2_AmmoFull"			"FULL"

"HL2_357Handgun"	".357 MAGNUM"
"HL2_Pulse_Rifle"	"OVERWATCH STANDARD ISSUE\n(PULSE-RIFLE)"
"HL2_Bugbait"		"PHEROPOD\n(BUGBAIT)"
"HL2_Crossbow"		"CROSSBOW"
"HL2_Crowbar"		"CROWBAR"
"HL2_Grenade"		"GRENADE"
"HL2_GravityGun"	"ZERO-POINT ENERGY GUN\n(GRAVITY GUN)"
"HL2_Pistol"		"9MM PISTOL"
"HL2_RPG"		"RPG\n(ROCKET PROPELLED GRENADE)"
"HL2_Shotgun"		"SHOTGUN"
"HL2_SMG1"		"SMG\n(SUBMACHINE GUN)"

"HL2_Saved"		"Saved..."
"HL2_Enable_Commentary"	"Enable commentary track"
"Valve_Hint_Crouch" "%+duck% CROUCH"
"Valve_Hint_Sprint" "%+speed% SPRINT"
"Valve_Hint_PushButton" "%+use% PUSH BUTTON"
"Valve_Hint_PicKUp" "%+use% PICK UP"
"Valve_Hint_Interact" "%+use% INTERACT"
"Valve_Hint_GravGun" "%+attack% PUNT OBJECT %+attack2% PULL OBJECT"
"Valve_Hint_CarryTurret" "%+use% OR GRAVITY GUN TO PICK UP TURRET"
"Valve_Hint_CROSSBOW" "%+attack2% CROSSBOW ZOOM"

"HL2_SAVING"						"Saving..."
} 
}




﻿A Combine official reviews annotated human memory of what transpired in City 12:

    COMBINE UNIVERSAL UNION

    #C12-DOC-004353

    REGARDING THE RECENT FAILURE
    REFERRED TO AS "ALTERED TRANSMISSION"

    November 9, 2027.

    City 12 is about to be free once more.

    - The resistance is operating openly and is rapidly regaining influence.
    - The Civil Protection is increasingly corrupt.
    - Anti-combine television broadcasts are tolerated.

    To avoid a full uprising, the Combine has agreed to formally restore human
    rights in City 12 and allow limited human self-governing. The treaty will be
    signed in just a few days.

    There's doubt the treaty will actually be signed. To put more pressure on
    the Combine, the resistance is about to assault key civil protection
    stations in the downtown area.

    Freeman unexpectedly arrives in City 12. Unsure what to do with him, they
    ask him to lead the assault on Sector V.

Altered Transmission is a single player mod for Half-Life 2: Episode Two.

This is Demo 10, a development snapshot of Altered Transmission. The mod is not
finished and probably never will be as I was too ambitious and it is too much
work realizing my vision for it. Nonetheless, with a strong focus on gameplay,
all the neccesary maps have been made and the mod is fully playable from start
to finish. There's no voice acting, but the story is told through subtle
environment details. A few maps are still have rough graphics, but they are all
playable. Please ignore any obviously unfinished aspects of the mod.

City 12 is loosely based on Århus, Denmark and many locations are based on the
real world.

To install, extract the mod directory into your Steam/steamapps/SourceMods
directory and if needed restart Steam. If you had any earlier version of this
mod installed, you need to remove them first. Finally from Steam launch the
mod Altered Transmission to play. You need to install Source SDK Base 2013
Singleplayer from Steam's Tools tab as well as Half-Life 2: Episode Two.

This is as gameplay test. I'd really like to know what you think of it. There's
lots of ways you can give me feedback:

 - Send a review to sortie@maxsi.org.
 - Add sortie as friend on Steam and interactively tell me while you play.
 - Record demo files in the engine that I can play back.
 - Publish a Let's Play of the mod.

Demo 10 is the first release since 2009, where development unfortunately hit
some bad times. I made too many invasive changes to maps that made it impossible
to play from start to end. Valve broke the SDK a couple of times. School and
later university required too much of my time. I moved from Windows to Linux. My
silly programming projects somehow evolved into a self-hosting Unix-like
operating system (sortix.org). Then a few failed reboots of development.

I persevered because I had a good deal of cool improvements to the mod that I
never had got around to release, and because I rather like what I have made so
far. Demo 10 comes with new maps and a whole lot of improvements to the existing
parts of the mod. I hope you like it too.